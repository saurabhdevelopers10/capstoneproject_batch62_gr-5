package com.stackroute.userservices.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.apache.commons.lang.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue
    private int id;
    @Column(unique=true)
    private String userName;
    private String email;
    private String password;
    private String qualification;

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public void copyWithoutNotNull(User user){
        if(StringUtils.isNotEmpty(user.getUserName())){
        this.userName= user.userName;
    }
    if(StringUtils.isNotEmpty(user.getEmail())){
        this.email= user.email;
    }
    if(StringUtils.isNotEmpty(user.getQualification())){
        this.qualification= user.qualification;
    }
    
    }
}
