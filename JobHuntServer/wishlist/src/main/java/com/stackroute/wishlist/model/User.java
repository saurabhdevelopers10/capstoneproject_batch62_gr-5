package com.stackroute.wishlist.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class User {
    @Id
    private String username;
    private List<Job> JobList;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<Job> getJobList() {
		return JobList;
	}
	public void setJobList(List<Job> jobList) {
		JobList = jobList;
	}
    public User() {
    	
    }
	public User(String username, List<Job> jobList) {
		super();
		this.username = username;
		JobList = jobList;
	}
    

}
